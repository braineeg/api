<?php

/**
 * Classe principal do sistema responsável por criar a interface padrão assim como verificar 
 * a sessão e permissões do usuário.
 * 
 * @author Marcio Bigolin - <marcio.bigolinn@gmail.com>
 * @version 1.0.0
 */
class ControladorGeral extends AbstractController
{

  
    public function paginaNaoEncontrada()
    {
        $this->view->setTitle('Você não devia ter chegado aqui!');
        $this->view->addTemplate('pagina404');
    }

    public function index()
    {
        $this->salvar();
    }
    
    public function salvar()
    {
        $data = json_decode($this->getArg(1));
        $dao = new RegistroDAO();
        foreach($data as $valor){
            $registro = new Registro();
            $registro->setIdAluno(1);
            $registro->setDataRegistro($valor->rt);
            $registro->setSinal($valor->id);
            $registro->setDado($valor->valor);
            $dao->save($registro);
        }
    }
}
