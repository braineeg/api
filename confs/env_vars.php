<?php

/* 
 * Copyright (c) 2017, marcio
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//putenv('DATABASE_URL=postgres://qfrwkgkfvqettg:763783cea910d102995873d860e17e32334ec991c8c4829d7e09d9de78090bc8@ec2-107-22-160-199.compute-1.amazonaws.com:5432/dfnsbvf8oi8lcp');
//$db = parse_url(getenv('DATABASE_URL'));

if(getenv('DATABASE_URL')){	
   $db = parse_url(getenv('DATABASE_URL'));
   putenv('DB_USER=' . $db['user']);
   putenv('DB_PASSWORD=' . $db['pass']);
   putenv('DB_NAME=' . ltrim($db['path'], '/'));
   putenv('DB_SERVER=' . $db['host']);
}else if(!file_exists(ROOT . '../.env')){
    //Ambiente de testes
    putenv('DB_USER=user');
    putenv('DB_PASSWORD=pass1234');
    putenv('DB_NAME=khonsu');
    putenv('DB_SERVER=khonsudb');
}
