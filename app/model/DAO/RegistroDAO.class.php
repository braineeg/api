<?php

/**
 * Classe de modelo referente ao objeto Registro para 
 * a manutenção dos dados no sistema 
 *
 * @package modulos.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 15-08-2019(Gerado automaticamente - GC - 1.1 11/07/2017)
 */

class RegistroDAO extends AbstractDAO 
{

    protected $table, $colunmID, $colunms;

    /**
    * Construtor da classe RegistroDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table = 'public.registro';
        $this->colunmID = 'id_registro';
        $this->colunms = array( 'id_aluno',
                                'sinal',
                                'dado',
                                'data_registro'
                               );
    }

    /**
     * Retorna um objeto setado Registro
     * com objetivo de servir as funções getTabela, getLista e getRegistro
     *
     * @param array $dados
     * @return objeto Registro
     */
    protected function setDados($dados)
    {
        $registro = new Registro();
        $registro->setIdRegistro($dados['principal']);
        $registro->setIdAluno($dados['id_aluno']);
        $registro->setSinal($dados['sinal']);
        $registro->setDado($dados['dado']);
        $registro->setDataRegistro($dados['data_registro']);
        return $registro;
    }
}