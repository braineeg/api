<?php
/**
 * Classe para a transferencia de dados de Registro entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 15-08-2019(Gerado Automaticamente com GC - 1.1 11/07/2017)
 */

 class Registro implements DTOInterface
 {
    use core\model\DTOTrait;

    private $idRegistro;
    private $idAluno;
    private $sinal;
    private $dado;
    private $dataRegistro;
    private $isValid;
    private $table;

    /**
     * Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param String $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.registro')
    {
        $this->table = $table;
    }

    /**
     * Retorna o valor da variável idRegistro
     *
     * @return Inteiro - Valor da variável idRegistro
     */
    public function getIdRegistro()
    {
        return $this->idRegistro;
    }

    /**
     * Método que seta o valor da variável idRegistro
     *
     * @param Inteiro $idRegistro - Valor da variável idRegistro
     */
    public function setIdRegistro($idRegistro)
    {
        $idRegistro = trim($idRegistro);
        if(empty($idRegistro)){
            $GLOBALS['ERROS'][] = 'O valor informado em Id registro não pode ser nulo!';
            return false;
        }
          if(!(is_numeric($idRegistro) && is_int($idRegistro + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Id registro é um número inteiro inválido!';
              return false;
          }
          $this->idRegistro = $idRegistro;
          return true;
    }

    /**
     * Retorna o valor da variável idAluno
     *
     * @return Inteiro - Valor da variável idAluno
     */
    public function getIdAluno()
    {
        return $this->idAluno;
    }

    /**
     * Método que seta o valor da variável idAluno
     *
     * @param Inteiro $idAluno - Valor da variável idAluno
     */
    public function setIdAluno($idAluno)
    {
        $idAluno = trim($idAluno);
          if(!(is_numeric($idAluno) && is_int($idAluno + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Id aluno é um número inteiro inválido!';
              return false;
          }
          $this->idAluno = $idAluno;
          return true;
    }

    /**
     * Retorna o valor da variável sinal
     *
     * @return String - Valor da variável sinal
     */
    public function getSinal()
    {
        return $this->sinal;
    }

    /**
     * Método que seta o valor da variável sinal
     *
     * @param String $sinal - Valor da variável sinal
     */
    public function setSinal($sinal)
    {
        $sinal = trim($sinal);
      $this->sinal = $sinal;
        return true;
    }

    /**
     * Retorna o valor da variável dado
     *
     * @return Inteiro - Valor da variável dado
     */
    public function getDado()
    {
        return $this->dado;
    }

    /**
     * Método que seta o valor da variável dado
     *
     * @param Inteiro $dado - Valor da variável dado
     */
    public function setDado($dado)
    {
        $dado = trim($dado);
          if(!(is_numeric($dado) && is_int($dado + 0))){
                $GLOBALS['ERROS'][] = 'O valor informado em Dado é um número inteiro inválido!';
              return false;
          }
          $this->dado = $dado;
          return true;
    }

    /**
     * Retorna o valor da variável dataRegistro
     *
     * @return String - Valor da variável dataRegistro
     */
    public function getDataRegistro()
    {
        return $this->dataRegistro;
    }

    /**
     * Retorna o valor da variável dataRegistro formatada 
     *
     * @param Bool $extenso - opção para retornar a data por extenso ou não 
     * @return String - Valor da variável dataRegistro formatada 
     */
    public function getDataRegistroFormatada($extenso = true)
     {
        return $extenso ? DateUtil::formataDataExtenso($this->dataRegistro) : DateUtil::formataData($this->dataRegistro);
    }

    /**
     * Método que seta o valor da variável dataRegistro
     *
     * @param String $dataRegistro - Valor da variável dataRegistro
     */
    public function setDataRegistro($dataRegistro)
    {
        $dataRegistro = trim($dataRegistro);
      $this->dataRegistro = $dataRegistro;
        return true;
    }

    /**
     * Retorna o valor da variável $tabela 
     *
     * @return String - Tabela do SGBD
     */
     public function getTable()
    {
        return $this->table;
     }

     public function setTable($table)
    {
        $this->table = $table;
     }

    /**
     * Responsável por retornar um array em formato JSON 
     * para poder ser utilizado como Objeto Java Script
     *
     * @return Array -  Array JSON
     */
     public function getArrayJSON()
     {
        return array(
             $this->idRegistro,
             $this->idAluno,
             $this->sinal,
             $this->dado,
             $this->dataRegistro
        );
     }


    /**
     * Utiliza como condição de seleção a chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
     public function getID(){
        return $this->idRegistro;
     }

    /**
     * Utiliza como condição de seleção a chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id_registro = ' . $this->idRegistro;
     }
}
