<?php

/**
 * Arquivo que apresenta as configurações de banco de dados.
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package
 */


//Constante que define o servidor
define('DB_SERVER', getenv('DB_SERVER'));

//Constante que define a porta do banco de dados
define('DB_PORT', '5433');

//Constante que define o usuário do banco de dados
define('DB_USER', getenv('DB_USER'));

//Constante que define o usuário do banco de dados
define('DB_PASSWORD', getenv('DB_PASSWORD'));

//Constante que define o usuário do banco de dados
define('DB_NAME', getenv('DB_NAME'));

define('DB_TYPE', 'pgsql');



